===========================================================
---- Resume -------------------------------- John Long ----
===========================================================

Denver, CO                                    GitHub: Adduc
john@128.io                                   GitLab: Adduc

===========================================================
------------------------- Summary -------------------------
===========================================================

Experienced backend developer and systems administrator
specialized in performance optimization across variants of
the typical LAMP stack. Cultivated a jack of all trades
skill set from working at small companies with growing
needs.

===========================================================
----------------- Professional Experience -----------------
===========================================================

Senior Backend Engineer                   08/2018 – 01/2020
Stitcher

- Refactored caching system, cutting response times by 15%
  and reducing number of cache calls by 40%
- Migrated entire company from subversion to git,
  establishing code reviews as part of development
  workflow
- Relevant technologies: Laravel, Lumen, MariaDB, Puppet,
  AWS EC2, AWS S3, Postgres, Redis, Memcache, PHPUnit,
  New Relic, Prometheus, Grafana

Web Developer                             05/2015 – 06/2018
RAM Racing

- Maintained a multi-tenant SaaS eCommerce platform for
  event organizers.
- Developed marketing sites backed by Salesforce data
  (custom static-site generator)
- Developed a real-time integration with Salesforce
  Marketing Cloud (REST and SOAP APIs, Automation Studio)
- Moved non-immediate tasks (email, reports, etc.) to
  queues, reducing 99th percentile response times by 80%.
- Developed CI/CD pipeline for automating deployments
- Performed regular infrastructure maintenance and upgrades
  (PHP 5 to 7)
- Relevant technologies: CakePHP, Laravel, ImageMagick,
  ZPL, GitLab, MariaDB, Docker, GitLab CI

LAMP Developer                            09/2014 - 05/2015
Cooper’s Hawk Winery & Restaurants

- Developed a point of sales system for purchasing and
  activating gift cards.
- Refactored from procedural-driven architecture to object-
  oriented, leading to 80% reduction in codebase size and
  40% reduction in defects per release.
- Relevant technologies: PHP, WolfCMS, Composer, Continuous
  Integration, Apache, LAMP, Jenkins, jQuery, Javascript,
  HTML, MySQL, Docker

Web Developer / Systems Administrator     11/2013 - 06/2014
Dealer e-Process

- Worked with database administrator to improve
  performance, reducing server page render time by 70%, and
  cutting required amount of servers by 50%
- Audited financial application for compliance in storage
  and accessibility of data.
- As technical lead, oversaw SEO efforts, including
  addition of micro-snippets, restructure of search, and
  communication of do's and do-not's to clients.
- Relevant technologies: PHP, CodeIgniter, Composer, Nginx,
  Apache, LAMP, Continuous Integration, Jenkins, SEO,
  MySQL, Percona, Docker, RHEL, CentOS, Ubuntu

Web Developer                             01/2011 – 11/2013
Carousel Checks

- Led development efforts to move e-commerce and internal
  order fulfillment platform into PCI-compliance.
- As technical lead, architected and developed a multi-
  tenant e-commerce system for financial institutions.
- Through architecture and development of variable-data
  printing software, reduced time to generate print jobs
  from 5 minutes to 30 seconds and reduced operator error
  by 10%.
- Relevant technologies: PHP, CakePHP, Composer, Nginx,
  Apache, LAMP, HTML, MySQL, jQuery, SEO, CentOS, PCI-DSS

===========================================================
-------------------- Personal Projects --------------------
===========================================================

Unofficial Feeds for Stitcher Premium	       2017 – Present

- Reverse engineered a service’s API and wrote an
  application to allow paid subscribers to listen to
  podcasts in the app of their choice.
- As of April 2020, there are over 3,900 users, serving
  1.1 million requests daily
- Relevant technologies: Laravel Lumen, MySQL, GitLab CI
- Site: https://stitcher-rss.128.io
- Source: https://gitlab.com/adduc-projects/stitcher-rss-2

Unofficial Feeds for Howl.FM                    2016 – 2019

- Reverse engineered a service’s API and wrote an
  application to allow paid subscribers to listen to
  podcasts in the app of their choice.
- Relevant technologies: Silex, Doctrine Cache