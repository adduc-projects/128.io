===========================================================
---- Blog: Entry 1234 from 2020-03-01 ------ John Long ----
===========================================================

Building a portfolio site




- text files (ascii files?)
- Inspired by Vulfpeck's website
- reddit post





===========================================================
-------------------------- Links --------------------------
===========================================================

Resume

  .docx
  https://128.io/resume.docx

  .pdf
  https://128.io/resume.pdf

  .txt
  https://128.io/resume.txt

Blog

  Entries
  https://128.io/blog

  Feed
  https://128.io/blog.rss                                               Add meta tags for feed

Projects

  Unofficial Feeds for Stitcher RSS
  https://stitcher-rss.128.io

Social Media

  GitHub:   https://github.com/adduc
  GitLab:   https://gitlab.com/adduc
  Twitter:  https://twitter.com/jlong_128io
  Mastodon: https://mastodon.technology/@adduc

About

  https://128.io/about

This Site

  Made with ♥️ by John Long
  Version: abcdef0123456789
  Source: https://gitlab.com/adduc-projects/128.io                      /* Inspired by Vulfpeck's website */