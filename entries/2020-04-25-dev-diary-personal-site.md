# Dev Diary: Designing a personal website --- 2020-04-25

Context:

  I have had a few failed starts at developing and
  maintaining a personal website. To simplify maintenance
  concerns, I wanted to build a primarily text-based
  website.

Initial Requirements:

- Create a personal website
- Site should be static, or staticly generated (no dynamic
  code on serve)

Build Log:

- I have had an appreciation for Vulfpeck's website
  (https://vulfpeck.com/), which is text-based with narrow
  columns. It was simple, but the site I wanted to make
  would have less information and would not work if it
  emulated Vulfpeck's website outright. I took a look at
  various files on http://textfiles.com for additional
  inspiration for styling the website.

- I was able to get layouts for the index, blog, and blog
  entry pages created by hand without too much trouble.
  Since the website was going to use a monospaced font, my
  text editor became a "WYSIWYG" editor as there was no need
  to preview changes in the browser.

- After creating the initial layouts, I attempted to convert
  them into templates to reduce duplicate sections (header,
  footer, etc.). During the process, I kept tweaking
  sections enough that templating was set aside. I intend to
  come back to templates later on, and kept the initial
  efforts in a separate directory to look at later on.

- It wasn't until late into the project that I first checked
  how it rendered on mobile. The arbitrary choice of
  limiting width to 60 characters with hard-coded wrapping
  caused vertical scrolling on mobile devices. I tried a few
  quick approaches to improve the rendering, but wasn't
  happy with any and reverted back. Switching from hard
  -coded wrapping to letting the browser determine when to
  wrap could work with some adjustments, but would prevent
  some text-styles from working in the future (ascii art
  that is wrapped by the browser would look horrible).

Ideas for the future:

- Create initial blog pages by hand.
- Create deployment script.
- Start to use PHP to replace headers, footers that are
  currently duplicated.
- Look at generating resume programatically to reduce
  duplicate content.
- Is it possible to make website look better on mobile?
  - Wrap using CSS? Use max width to ensure it looks good
    on desktop?
  - How to render header? tab indentations? right align?

Relevant links:

- https://vulfpeck.com
- http://textfiles.com
- https://old.reddit.com/r/textfiles/
- https://old.reddit.com/r/LightWeightMarkup/